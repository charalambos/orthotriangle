#include <iostream>
#include <GL/glew.h>            ///Include GLEW header files
#include <GLFW/glfw3.h>         ///Include GLFW header files
#include <fstream>
#include <vector>

#define VIEWER_WIDTH    800
#define VIEWER_HEIGHT   800

///The handle to the window
GLFWwindow *window_handle = 0x00;
///IDs of shaders
GLuint vertex_shader_id = 0;
GLuint fragment_shader_id = 0;
GLuint shader_program_id = 0;
///ID for the VAO, VBO
GLuint vao = 0;
GLuint vbo = 0;

GLuint loadShaders(std::string vertex_shader_path, std::string fragment_shader_path) {

    // Create the shaders
    vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
    fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_shader_path.c_str(), std::ios::in);

    if (VertexShaderStream.is_open()) {
        std::string Line = "";
        while (getline(VertexShaderStream, Line))
            VertexShaderCode += "\n" + Line;
        VertexShaderStream.close();
    }
    else {
        printf("Impossible to open %s. Are you in the right directory ?\n", vertex_shader_path.c_str());
        getchar();
        exit(-1);
    }

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_shader_path.c_str(), std::ios::in);

    if (FragmentShaderStream.is_open()) {
        std::string Line = "";
        while (getline(FragmentShaderStream, Line))
            FragmentShaderCode += "\n" + Line;
        FragmentShaderStream.close();
    }
    else {
        printf("Impossible to open %s. Are you in the right directory ?\n", fragment_shader_path.c_str());
        getchar();
        exit(-1);
    }

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_shader_path.c_str());
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(vertex_shader_id, 1, &VertexSourcePointer, 0x00);
    glCompileShader(vertex_shader_id);

    // Check Vertex Shader
    glGetShaderiv(vertex_shader_id, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(vertex_shader_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (InfoLogLength > 0) {
        std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
        glGetShaderInfoLog(vertex_shader_id, InfoLogLength, 0x00, &VertexShaderErrorMessage[0]);
        printf("%s\n", &VertexShaderErrorMessage[0]);
    }

    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_shader_path.c_str());
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(fragment_shader_id, 1, &FragmentSourcePointer, 0x00);
    glCompileShader(fragment_shader_id);

    // Check Fragment Shader
    glGetShaderiv(fragment_shader_id, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(fragment_shader_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (InfoLogLength > 0) {
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
        glGetShaderInfoLog(fragment_shader_id, InfoLogLength, 0x00, &FragmentShaderErrorMessage[0]);
        printf("%s\n", &FragmentShaderErrorMessage[0]);
    }

    // Link the program
    printf("Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, vertex_shader_id);
    glAttachShader(ProgramID, fragment_shader_id);

    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (InfoLogLength > 0) {
        std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, 0x00, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }
    return ProgramID;
}


bool initialize()   {

    ///Initialize the GLFW window
    if (!glfwInit()) return false;

    ///Create the window
    window_handle = glfwCreateWindow(VIEWER_WIDTH, VIEWER_HEIGHT, "COMP371 - OrthoTriangle",0x00,0x00);
    if (!window_handle)    {
        std::cout << "ERROR: Failed to create a window." << std::endl;
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(window_handle);

    ///Initialize GLEW extension handler
    glewExperimental = GL_TRUE; ///Needed to get the latest version of OpenGL
    glewInit();

    ///Test if all is good
    const GLubyte  *renderer = glGetString(GL_RENDERER);
    std::cout << "Renderer: " << renderer << std::endl;
    const GLubyte  *version = glGetString(GL_VERSION);
    std::cout << "OpenGL version: " << version << std::endl;

    ///Enable the depth test
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);


    return true;
}

bool cleanup()  {
    //Release memory e.g. vao, vbo, etc
    glDeleteProgram(shader_program_id);
    glDeleteShader(vertex_shader_id);
    glDeleteShader(fragment_shader_id);

    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);

    if (window_handle)     glfwDestroyWindow(window_handle);
    window_handle = 0x00;

    glfwTerminate();

    return true;
}

int main() {
    std::cout << "Basic C++ & OpenGL: OrthoTriangle" << std::endl;

    ///initialize
    initialize();

    ///load the shaders
    shader_program_id = loadShaders("../basic.vs", "../basic.fs");

    ///define geometry
    float points[] =    {0.0f, 0.5f, 0.0f,
                         0.5f, -0.5f, 0.0f,
                         -0.5f,-0.5f,0.0f};

    ///setup the containers
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, 9*sizeof(float), points, GL_STATIC_DRAW);
    GLuint vertex_position_id = glGetAttribLocation(shader_program_id, "vertex_position");
    glVertexAttribPointer(vertex_position_id, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(vertex_position_id);

    ///start using the shader program
    glUseProgram(shader_program_id);

    std::cout << "Rendering..." << std::endl;
    while (!glfwWindowShouldClose(window_handle))  {
        ///erase the contents every time
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        ///draw points 0-3 from the currently bound VAO with current shader program
        glDrawArrays(GL_TRIANGLES, 0, 3);
        ///update other events like input handling
        glfwPollEvents();
        ///swap the buffers
        glfwSwapBuffers(window_handle);
    }

    ///cleanup
    cleanup();

    return 0;
}